# simple_behave_appium_android

Test project over Live de Python app
https://github.com/dunossauro/toga-android-login


## Como rodar os cenários de teste

### Subir AVD + Appium docker
```
docker run --privileged -d -p 6080:6080 -p 4723:4723 -p 5554:5554 -p 5555:5555 -v $PWD/example/sample_apk:/root/tmp -e DEVICE="Nexus 5" -e APPIUM=true -e APPIUM_HOST="127.0.0.1" -e APPIUM_PORT=4723 --name android-container budtmo/docker-android-x86-8.1
```
*OBS:* é importante se atentar a flag `-v $PWD/example/sample_apk:/root/tmp`, pois
utilizando da forma como está é necessário subir o docker na raiz do projeto
caso contrário é preciso alterar a flag para indicar onde o APK se encontra.

### Ativar o ambiente virtual
Instalar dendências:
`poetry install`

Ativar o embiente virtual:
`poetry shell`

Executar o behave:
`behave integrationtests/features`

class MainScreen:
    """Objeto da view principal do app."""

    def __init__(self, driver):
        """Inicia com o driver."""
        self.driver = driver

    def logint_btn(self) -> None:
        """Clica no botão LOGIN."""
        self.driver.find_element_by_class_name("android.widget.Button").click()

    def input_fields(self) -> list:
        """Pega todos os campos de input."""
        page_inputs = [
            input_ for input_ in self.driver.find_elements_by_class_name(
                "android.widget.EditText"
            )
        ]
        return page_inputs

    def page_text_view(self) -> list:
        page_views = [
            view for view in self.driver.find_elements_by_class_name(
                "android.widget.TextView"
            )
        ]
        return page_views

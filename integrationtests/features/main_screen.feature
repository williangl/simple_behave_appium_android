# language: pt
# encoding: utf-8

Funcionalidade: Tela Principal

  Contexto: Abrir o aplicativo
    Dado que esteja na tela principal do aplicativo


  Cenário: O titulo da pagina deve estar presente
    Dado que o texto "Live de Python" deve estar presente na página
    E o texto "Email" deve estar presente na página
    Então o texto "Senha" deve estar presente na página



  Cenário: Inserção de email e sennha
    Dado que preencho o campo de "email" com o dado "teste@teste.com"
    E que preencho o campo de "senha" com o dado "12345"
    Quando clico no botão de login
    Então nada deve acontecer


  Cenário: Digitar senha errada deve causar erro
    Dado que preencho o campo de "email" com o dado "teste@teste.com"
    E que preencho o campo de "senha" com o dado "errado"
    Quando clico no botão de login
    Então deve aparecer a mensagem de erro

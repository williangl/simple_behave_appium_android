"""Hooks file."""
from json import load
from logging import config, getLogger
from os import makedirs
from os.path import isdir

from appium import webdriver
from behave.tag_matcher import ActiveTagMatcher
from ipdb import post_mortem

from integrationtests.helpers import constants

active_tag_value_provider = {
    "config_0": False
}

active_tag_matcher = ActiveTagMatcher(active_tag_value_provider)


def before_all(context):
    context.userdata = context.config.userdata
    context.config_0 = context.userdata.get('config_0', 'False')
    logger_type = context.userdata.get('logger', 'file_logger')
    context.logger = setup_logger(logger_type)


def before_feature(context, feature):
    context.driver = webdriver.Remote("http://localhost:4723/wd/hub",
        {
            "app": context.userdata.get('app', ''),
            "platformName": "Android",
            "automationName": "UiAutomator1",
            "deviceName": context.userdata.get('device_name', 'Android Emulator'),
            "platformVersion": context.userdata.get('platform_version', '8.1'),
            'adbExecTimeout': 30000
        }
    )


def before_scenario(context, scenario):
    if active_tag_matcher.should_exclude_with(scenario.effective_tags):
        scenario.skip(reason="DISABLED ACTIVE-TAG")


def before_tag(context, tag):
    pass


def after_step(context, step):
    if context.config.userdata.get('debug') and step.status == "failed":
        post_mortem(step.exc_traceback)


def after_tag(context, tag):
    pass


def after_scenario(context, scenario):
    pass

def after_feature(context, feature):
    context.driver.quit()


def after_all(context):
    pass


def setup_logger(logger_name):
    if not isdir(constants.LOG_FILE_DIR):
        makedirs(constants.LOG_FILE_DIR)

    with open(constants.LOGGER_CONFIG, 'rt') as f:
        options = load(f)

    config.dictConfig(options)
    return getLogger(logger_name)

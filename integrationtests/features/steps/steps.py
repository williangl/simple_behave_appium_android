from behave import given, then, step, when

from integrationtests.modules.po_main import MainScreen


@given('que esteja na tela principal do aplicativo')
def main_window_application(context):
    main_window = MainScreen(context.driver)


@then('nada deve acontecer')
def simpl_step(context):
    assert True, 'Ops, alguma coisa aconteceu'


@when('clico no botão de login')
def login_btn_click(context):
    main_window = MainScreen(context.driver)
    main_window.logint_btn()

@given('que o texto "{texto}" deve estar presente na página')
@step('o texto "{texto}" deve estar presente na página')
def page_title(context, texto):
    main_window = MainScreen(context.driver)

    page_texts = {
        text_.text.lower(): text_ for text_ in main_window.page_text_view()
    }

    assert page_texts[texto.lower()].text.lower() == texto.lower(), \
        f'O texto na página é diferente do esperado.\
         Titulo: {textos_pagina[texto.lower()]} Esperado: {texto}'


@step('que preencho o campo de "{campo}" com o dado "{valor}"')
def insert_value(context, campo, valor):
    main_window = MainScreen(context.driver)
    page_fields = main_window.input_fields()

    if campo == 'email':
        page_fields[0].send_keys(valor)

    page_fields[1].send_keys(valor)


@then('deve aparecer a mensagem de erro')
def error_message(context):
    main_window = MainScreen(context.driver)
    message_field = main_window.input_fields()[2]
    assert message_field.text.lower() == 'erro', f'Não causou erro no login.\
     Esperado: "erro", Recebido: "{message_field.text}"'
